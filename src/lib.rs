//! Backup running machine with btrfs to backup drives

// TODO: VERY IMPORTANT!!! Delete old backups based on date, not on number of backups
// TODO: Do for loops instead of single assignments
// TODO: Move Config struct or Destination into Subvolume
// TODO: Turn remote stuff into a Trait for Subvolume
// TODO: Add proper errors via thiserror
// TODO: Add ANSI colors to text output
// TODO: Add backup count when doing dry run and normal run
// TODO: Add option to do scrub if necessary, i.e., autocheck for 2 weeks since last scrub, do if it's been 2 weeks, check/wait before shutdown
// TODO: Add log file output, possibly with subcommand/flag to view it in color (probably don't need)
// TODO: Add daily/weekly/monthly retention logic (creation and deletion)
// TODO: Add first logic to do first run, so full copy without diff

// Extra possible features
// TODO: Check available space in /boot before starting, notify user, do you want to continue? (This is for kernel deletion script)
// TODO: Check size of eclean-dist and eclean-pkg and see if we need to remove old stuff (Proc::Async?)
// TODO: Automatically launch tmux and update title bar with current status as you go along

#![warn(missing_docs)]

/// Configuration
pub mod config;

use ansi_term::Color::{Green, Red, Yellow};
use anyhow::{anyhow, bail, Result};
use chrono::{DateTime, Duration, FixedOffset, Local, SecondsFormat};
use clap::{ArgGroup, Parser};
use config::*;
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use std::cmp::Ordering;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};
use users::get_current_uid;

/// Parse CLI options
pub static OPTS: Lazy<Opts> = Lazy::new(Opts::parse);

/// Command line options
#[derive(Parser, Debug)]
#[clap(
    version,
    about = "Backup btrfs snapshots to local and remote backup drives"
)]
#[clap(group(ArgGroup::new("dest").required(true).multiple(true).args(&["localhost", "local", "remote"])))]
pub struct Opts {
    /// Include local drives in backup
    #[clap(short, long)]
    pub localhost: bool,

    /// Include backup drives in backup
    #[clap(short = 'L', long)]
    pub local: bool,

    /// Include remote drives in backup
    #[clap(short, long)]
    pub remote: bool,

    /// Delete old backups
    #[clap(short = 'd', long)]
    pub delete: bool,

    /// Don't actually run any backups
    #[clap(short = 'n', long)]
    pub dry_run: bool,

    /// Run scrub on all destination drives
    #[clap(short = 'S', long)]
    pub scrub: bool,

    /// Shutdown after backup is complete
    #[clap(short, long)]
    pub shutdown: bool,

    /// Verbose output
    #[clap(short, long)]
    pub verbose: bool,
}

/// Location of snapshots for shell commands
#[derive(Serialize, Deserialize, Debug, Copy, Clone, Eq, PartialEq)]
pub enum Location {
    /// Btrfs snapshots on local machine
    Localhost,

    /// Backup drive is located locally
    Local,

    /// Backup drive is located remotely
    Remote,
}

/// All snapshots to manipulate
#[derive(Debug, Clone)]
pub struct Snapshots {
    /// Subvolumes
    pub subvolumes: Vec<Subvolume>,

    /// Location
    pub location: Location,
}

/// Subvolume with available snapshots
#[derive(Debug, Clone)]
pub struct Subvolume {
    /// Btrfs snapshot
    pub snapshots: Vec<Snapshot>,

    /// Subvolume drive
    pub name: String,
}

/// btrfs snapshot
#[derive(Eq, Ord, PartialEq, PartialOrd, Debug, Clone)]
pub struct Snapshot {
    /// Path to snapshot
    pub path: PathBuf,

    /// Date of snapshot
    pub datetime: DateTime<FixedOffset>,
}

impl Snapshot {
    fn new(path: PathBuf) -> Self {
        let file_name = path.file_name().unwrap().to_str().unwrap();
        let string: &str = file_name.split('_').next().unwrap().trim();
        let datetime = DateTime::parse_from_rfc3339(string).unwrap();
        Snapshot { path, datetime }
    }
}

impl Subvolume {
    fn latest_datetime(&self) -> Result<DateTime<FixedOffset>> {
        match self.snapshots.last() {
            Some(last) => Ok(last.datetime),
            None => bail!("No snapshots found."),
        }
    }
}

impl Snapshots {
    /// Create collection of sorted snapshot lists
    pub fn new(location: Location) -> Result<Option<Self>> {
        let subvols: Result<Vec<Subvolume>> = CONFIG
            .sub_paths(location)
            .iter()
            .map(|(name, subvol)| find_snapshots(location, name, subvol))
            .collect();
        let mut subvols = subvols?;
        sort_snapshots(&mut subvols);

        Ok(Some(Snapshots {
            subvolumes: subvols,
            location,
        }))
    }

    /// Perform backup on all snapshots from drive
    pub fn backup(&mut self, mut localhost: Snapshots) -> Result<()> {
        localhost.latest(self)?;
        let dest = CONFIG.directory(self.location);
        for subvol in localhost.subvolumes {
            let dest = dest.join(subvol.name);
            let snapshots = &subvol.snapshots;
            snapshots
                .iter()
                .zip(snapshots.iter().skip(1))
                .for_each(|(prev, curr)| match self.location {
                    Location::Local => {
                        if let Err(e) = btrfs_send(prev, curr, &dest) {
                            eprintln!("{:#?}", e);
                        }
                    }
                    Location::Remote => {
                        if let Err(e) = ssh_btrfs_send(prev, curr, &dest) {
                            eprintln!("{:#?}", e);
                        }
                    }
                    Location::Localhost => {
                        eprintln!("Trying to back up to localhost destination...");
                    }
                });
        }
        Ok(())
    }

    /// Remove snapshots from before latest backup/remote snapshot
    pub fn latest(&mut self, backup_drive: &mut Snapshots) -> Result<()> {
        let lasts: Result<Vec<DateTime<FixedOffset>>> = backup_drive
            .subvolumes
            .iter()
            .map(|subvol| subvol.latest_datetime())
            .collect();
        let lasts = lasts?;
        let latest_timestamp = if lasts.iter().all(|&d| lasts[0] == d) {
            lasts.get(0).unwrap().to_owned()
        } else {
            bail!("Subvolume latest snapshot datetimes don't match.");
        };

        let latest_pos = self.snapshot_position(latest_timestamp)?;
        for subvol in self.subvolumes.iter_mut() {
            subvol.snapshots = (subvol.snapshots[latest_pos..]).to_vec();
        }

        Ok(())
    }

    fn snapshot_position(&mut self, latest_timestamp: DateTime<FixedOffset>) -> Result<usize> {
        let latest_pos: Vec<usize> = self
            .subvolumes
            .iter()
            .map(|subvol| {
                subvol
                    .snapshots
                    .iter()
                    .position(|snap| snap.datetime == latest_timestamp)
                    .unwrap()
            })
            .collect();
        if !latest_pos.iter().all(|&b| latest_pos[0] == b) {
            bail!("latest_pos don't all match.");
        }
        let latest_pos = *latest_pos.first().unwrap();
        Ok(latest_pos)
    }

    /// Delete old snapshots to retention limit
    pub fn delete_old(&mut self, location: Location) -> Result<()> {
        dbg!("Made it into delete_old");

        let today = Local::today();

        for subvol in self.subvolumes.iter() {
            let temp_retention = match location {
                Location::Localhost => 21,
                Location::Local => 21,
                Location::Remote => 21,
            };
            let mut subvol_len = subvol.snapshots.len();
            let limit = today
                .checked_sub_signed(Duration::days(temp_retention as i64))
                .unwrap();
            dbg!(today);
            dbg!(limit);

            for snapshot in subvol.snapshots.iter() {
                let snap_date = DateTime::<Local>::from(snapshot.datetime);
                dbg!(snap_date.date());

                if snap_date.date().cmp(&limit) == Ordering::Less {
                    if subvol_len <= temp_retention {
                        info(&format!("subvol: {}", subvol.name));
                        alert(&format!("    Current snapshots: {subvol_len}"));
                        alert(&format!("    Snapshot limit: {temp_retention}"));
                        alert("    Not deleting any old snapshots.");
                        break;
                    } else {
                        subvol_len -= 1;
                    }
                    delete_snapshot(&location, snapshot)?;
                }
            }
        }

        Ok(())
    }
}

/// Error and quit if not running as root user
pub fn root_user_check() -> Result<()> {
    if get_current_uid() != 0 {
        bail!("Not running as root user");
    }
    Ok(())
}

/// Create timestamp for new backups
pub fn create_timestamp() -> String {
    let timestamp = Local::now();
    timestamp.to_rfc3339_opts(SecondsFormat::Secs, true)
}

/// Mount /btrfs to localhost snapshot access
pub fn mount_btrfs() -> Result<()> {
    let status = Command::new("mount")
        .arg(CONFIG.localhost_btrfs.as_os_str())
        .status()?;
    if status.success() {
        Ok(())
    } else {
        bail!("Failed to mount /btrfs")
    }
}

/// Unmount /btrfs
pub fn unmount_btrfs() -> Result<()> {
    let status = Command::new("umount")
        .arg(CONFIG.localhost_btrfs.as_os_str())
        .status()?;
    if status.success() {
        Ok(())
    } else {
        bail!("Failed to mount /btrfs")
    }
}

/// Take snapshots of all localhost subvolumes
pub fn create_localhost_snapshots(timestamp: &str) -> Result<()> {
    for (subvol_name, sub_path) in CONFIG.sub_paths(Location::Localhost) {
        if OPTS.dry_run {
            println!(
                "btrfs subvolume snapshot -r {}/{} {}/{}_daily",
                CONFIG.localhost_btrfs.display(),
                subvol_name,
                sub_path.display(),
                timestamp
            );
        } else {
            let status = Command::new("btrfs")
                .args(&[
                    "subvolume",
                    "snapshot",
                    "-r",
                    &format!("{}/{}", CONFIG.localhost_btrfs.display(), subvol_name),
                    &format!("{}/{}_daily", sub_path.display(), timestamp),
                ])
                .status()?;
            if !status.success() {
                bail!("Failed to take localhost snapshots.");
            }
        }
    }
    Ok(())
}

/// Find all snapshot paths for single subvolume
pub fn find_snapshots(location: Location, name: &str, path: &Path) -> Result<Subvolume> {
    if location == Location::Localhost || location == Location::Local {
        let iter = std::fs::read_dir(&path)?;
        let files: Vec<Snapshot> = iter.map(|x| Snapshot::new(x.unwrap().path())).collect();
        Ok(Subvolume {
            name: name.to_string(),
            snapshots: files,
        })
    } else if location == Location::Remote {
        let output = Command::new("ssh")
            .arg("mercure-root")
            .arg(format!("ls -d {}/*", path.display()))
            .output()?;
        let string = String::from_utf8(output.stdout)?;
        let files: Vec<Snapshot> = string
            .trim()
            .split('\n')
            .map(|x| {
                if OPTS.verbose {
                    dbg!(x);
                }
                Snapshot::new(PathBuf::from(x.trim()))
            })
            .collect();
        if OPTS.verbose {
            dbg!(&string);
            dbg!(&files);
        }
        Ok(Subvolume {
            name: name.to_string(),
            snapshots: files,
        })
    } else {
        bail!("Path not allowed for finding snapshots.")
    }
}

/// Sort array of snapshots by descending datetime
pub fn sort_snapshots(subvols: &mut [Subvolume]) {
    for subvol in subvols {
        subvol.snapshots.sort_by(|a, b| a.datetime.cmp(&b.datetime));
    }
}

// / Send incremental snapshots to backup destination
// pub fn backup_snapshots(snapshots: &[Snapshot], path: &Path) -> Result<()> {}

/// Send btrfs incremental snapshots
//TODO
fn btrfs_send(prev: &Snapshot, curr: &Snapshot, dest: &Path) -> Result<()> {
    let prev_path = prev
        .path
        .to_str()
        .ok_or_else(|| anyhow!("Failed to get prev path"))?;
    let curr_path = curr
        .path
        .to_str()
        .ok_or_else(|| anyhow!("Failed to get curr path"))?;

    let dest_path = dest
        .to_str()
        .ok_or_else(|| anyhow!("Failed to get dest path"))?;

    if OPTS.dry_run {
        println!(
            "btrfs send -p {} {} | btrfs receive {}",
            prev.path.display(),
            curr.path.display(),
            dest_path
        );
        return Ok(());
    }

    let command = Command::new("btrfs")
        .args(&["send", "-p", prev_path, curr_path])
        .stdout(Stdio::piped())
        .spawn()?;

    let command2 = Command::new("btrfs")
        .arg("receive")
        .arg(dest_path)
        .stdin(command.stdout.unwrap())
        .status()?;

    if !command2.success() {
        bail!("Failed to send backup: {}", prev.path.to_str().unwrap());
    }
    Ok(())
}

/// Send btrfs incremental snapshots
fn ssh_btrfs_send(prev: &Snapshot, curr: &Snapshot, dest: &Path) -> Result<()> {
    let prev_path = prev
        .path
        .to_str()
        .ok_or_else(|| anyhow!("Failed to get prev path"))?;
    let curr_path = curr
        .path
        .to_str()
        .ok_or_else(|| anyhow!("Failed to get curr path"))?;

    let dest_path = dest
        .to_str()
        .ok_or_else(|| anyhow!("Failed to get dest path"))?;

    if OPTS.dry_run {
        println!(
            "btrfs send -p {} {} | ssh mercure-root \"btrfs receive {}\"",
            prev.path.display(),
            curr.path.display(),
            dest_path
        );
        return Ok(());
    }

    let command = Command::new("btrfs")
        .args(&["send", "-p", prev_path, curr_path])
        .stdout(Stdio::piped())
        .spawn()?;

    if OPTS.verbose {
        dbg!(dest_path);
    }

    let command2 = Command::new("ssh")
        .arg("mercure-root") // TODO: Make this a config variable
        .arg(&format!("btrfs receive {}", dest_path))
        .stdin(command.stdout.unwrap())
        .status()?;

    if !command2.success() {
        bail!("Failed to send backup: {}", prev.path.to_str().unwrap());
    }
    Ok(())
}

/// Delete btrfs snapshot
pub fn delete_snapshot(location: &Location, snapshot: &Snapshot) -> Result<()> {
    let command = format!("btrfs subvolume delete {}", snapshot.path.to_str().unwrap());
    let error = "Could not delete snapshot.";
    match location {
        Location::Localhost => run_command(&command, error)?,
        Location::Local => run_command(&command, error)?,
        Location::Remote => run_ssh_command(&command, error)?,
    }
    Ok(())
}

/// Run local shell command
fn run_command(string: &str, error: &'static str) -> Result<()> {
    if OPTS.dry_run {
        println!("{}", string);
        return Ok(());
    }

    let vec: Vec<&str> = string.split(' ').collect();
    let status = Command::new(vec[0]).args(&vec[1..]).status()?;

    if !status.success() {
        bail!(error);
    }
    Ok(())
}

//TODO: Batch the deletions, so only one SSH command
/// Run remote shell command
pub fn run_ssh_command(string: &str, error: &'static str) -> Result<()> {
    if OPTS.dry_run {
        println!("ssh mercure-root \"{}\"", string);
        return Ok(());
    }

    let ssh = Command::new("ssh")
        .arg("mercure-root")
        .arg(string)
        .status()?;

    if !ssh.success() {
        bail!("ssh command \"{}\" failed: {}", string, error);
    }
    Ok(())
}

/// Shutdown laptop
pub fn shutdown() -> Result<()> {
    let status = Command::new("shutdown").arg("-h").arg("+2").status()?;
    if status.success() {
        Ok(())
    } else {
        bail!("Failed to shutdown")
    }
}

/// Print text in green
pub fn info(string: &str) {
    println!("{}", Green.bold().paint(string));
}

/// Print text in yellow
pub fn alert(string: &str) {
    println!("{}", Yellow.bold().paint(string));
}

/// Print text in red
pub fn warning(string: &str) {
    println!("{}", Red.bold().paint(string));
}
