use anyhow::{Context, Result};
use backup::config::*;
use backup::*;

fn main() -> Result<()> {
    // Workaround to make script stop after --help
    let _ = *OPTS;
    root_user_check()?;
    check_config()?;
    mount_btrfs()?;
    // TODO
    // check_destination_mounts()?;

    if OPTS.localhost {
        println!();
        info("Creating localhost snapshots.");
        let timestamp = create_timestamp();
        create_localhost_snapshots(&timestamp).unwrap_or_else(|_| unmount_btrfs().unwrap());
    }
    let localhost =
        Snapshots::new(Location::Localhost)?.expect("Unable to parse localhost snapshots.");
    if OPTS.local {
        let local = Snapshots::new(Location::Local)?;
        if let Some(mut local_snaps) = local {
            println!();
            info("Backing up local drives.");
            local_snaps
                .backup(localhost.clone())
                .unwrap_or_else(|_| unmount_btrfs().expect("Unable to get local snapshots."));
        }
    }

    if OPTS.remote {
        let remote = Snapshots::new(Location::Remote)?;
        if let Some(mut remote_snaps) = remote {
            println!();
            info("Backing up remote drives.");
            remote_snaps
                .backup(localhost)
                .unwrap_or_else(|_| unmount_btrfs().expect("Unable to get remote snapshots."));
        }
    }

    if OPTS.delete {
        if OPTS.localhost {
            let mut localhost = Snapshots::new(Location::Localhost)
                .context("Unable to parse localhost snapshots.")?
                .context("No localhost snapshots found.")?;

            println!();
            alert("Deleting old localhost backups.");
            localhost
                .delete_old(Location::Localhost)
                .context("Unable to delete localhost snapshots.")?;
        }

        if OPTS.local {
            let local =
                Snapshots::new(Location::Local).context("Unable to parse local snapshots.")?;
            if let Some(mut local_snaps) = local {
                println!();
                alert("Deleting old local backups.");
                local_snaps
                    .delete_old(Location::Local)
                    .context("Unable to delete local snapshots.")?;
            }
        }

        if OPTS.remote {
            let remote =
                Snapshots::new(Location::Remote).context("Unable to parse remote snapshots.")?;
            if let Some(mut remote_snaps) = remote {
                println!();
                alert("Deleting old remote backups.");
                remote_snaps
                    .delete_old(Location::Remote)
                    .context("Unable to delete remote snapshots.")?;
            }
        }
    }

    unmount_btrfs()?;

    if OPTS.shutdown && !OPTS.dry_run {
        alert("Shutting down in 2 minutes.");
        shutdown()?;
    }

    Ok(())
}
