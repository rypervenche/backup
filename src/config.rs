// TODO: Variablize $src and $dest in mount_commands and rsync
#![warn(missing_docs)]
use crate::Location;
use anyhow::Result;
use clap::crate_name;
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use std::default::Default;
use std::fs;
use std::path::PathBuf;

/// Parse config file
pub static CONFIG: Lazy<Config> = Lazy::new(|| read_config().expect("Failed to read config file."));

/// Path to configuration file
pub static CONFIG_FILE_PATH: Lazy<PathBuf> = Lazy::new(|| {
    if let Some(config_dir) = dirs::config_dir() {
        let mut app_dir = config_dir.join(crate_name!()).join(crate_name!());
        app_dir.set_extension("yaml");
        app_dir
    } else {
        panic!("Cannot set config directory");
    }
});

/// Configuration via YAML file
#[derive(Debug, Deserialize, Serialize)]
pub struct Config {
    /// Localhost btrfs mount
    pub localhost_btrfs: PathBuf,

    /// List of subvolumes to back up
    pub subvolumes: Vec<Subvolume>,

    /// Backup destinations to send snapshots to
    pub destinations: Vec<Destination>,
}

/// Information pertaining to backup destination
#[derive(Debug, Deserialize, Serialize)]
pub struct Destination {
    /// Backup drive location
    pub location: Location,

    /// SSH hostname
    pub ssh_hostname: Option<String>,

    /// Path to backup destination
    pub directory: PathBuf,

    /// Retention information
    pub retention: Retention,
}

impl Destination {
    /// Full path to destination's snapshot directory
    pub fn snap_dir(&self, dest: &str) -> PathBuf {
        self.directory.join(dest)
    }
}

/// Btrfs subvolume
#[derive(Debug, Deserialize, Serialize)]
pub struct Subvolume {
    /// Subvolume name
    pub name: String,
}

/// Retention information
#[derive(Debug, Deserialize, Serialize)]
pub struct Retention {
    /// Number of daily backups to retain
    pub daily: u8,

    /// Number of weekly backups to retain
    pub weekly: u8,

    /// Number of monthly backups to retain
    pub monthly: u8,
}

impl Config {
    /// Full paths to desired subvolumes
    pub fn sub_paths(&self, location: crate::Location) -> Vec<(String, PathBuf)> {
        self.subvolumes
            .iter()
            .map(|s| match location {
                Location::Localhost => (
                    s.name.clone(),
                    self.destinations
                        .iter()
                        .find(|d| d.location == Location::Localhost)
                        .unwrap()
                        .directory
                        .join(&s.name),
                ),
                Location::Local => (
                    s.name.clone(),
                    self.destinations
                        .iter()
                        .find(|d| d.location == Location::Local)
                        .unwrap()
                        .directory
                        .join(&s.name),
                ),
                Location::Remote => (
                    s.name.clone(),
                    self.destinations
                        .iter()
                        .find(|d| d.location == Location::Remote)
                        .unwrap()
                        .directory
                        .join(&s.name),
                ),
            })
            .collect()
    }

    /// Directory for given location
    pub fn directory(&self, location: crate::Location) -> PathBuf {
        self.destinations
            .iter()
            .find(|d| d.location == location)
            .unwrap()
            .directory
            .to_owned()
    }
}

impl Default for Config {
    fn default() -> Self {
        Config {
            localhost_btrfs: PathBuf::from("/btrfs"),
            subvolumes: vec![
                Subvolume {
                    name: "root".to_string(),
                },
                Subvolume {
                    name: "home".to_string(),
                },
            ],
            destinations: vec![
                Destination {
                    location: Location::Localhost,
                    ssh_hostname: None,
                    directory: PathBuf::from("/btrfs/snapshots"),
                    retention: Retention {
                        daily: 7,
                        weekly: 4,
                        monthly: 6,
                    },
                },
                Destination {
                    location: Location::Local,
                    ssh_hostname: None,
                    directory: PathBuf::from("/mnt/backup/jais"),
                    retention: Retention {
                        daily: 7,
                        weekly: 4,
                        monthly: 6,
                    },
                },
                Destination {
                    location: Location::Remote,
                    ssh_hostname: Some("mercure-root".to_string()),
                    directory: PathBuf::from("/mnt/backup/jais"),
                    retention: Retention {
                        daily: 7,
                        weekly: 4,
                        monthly: 6,
                    },
                },
            ],
        }
    }
}

/// Create configuration file with defaults
pub fn create_config(output: &str) -> Result<()> {
    let parent = CONFIG_FILE_PATH.parent().unwrap();
    fs::create_dir_all(parent)?;
    fs::write(&*CONFIG_FILE_PATH, output)?;
    Ok(())
}

/// Create config file if doesn't already exist
pub fn check_config() -> Result<()> {
    if !CONFIG_FILE_PATH.is_file() {
        let default_config = Config::default();
        let output = serde_yaml::to_string(&default_config)?;
        create_config(&output)?;
        println!(
            "New config file created at {}.\nPlease edit it to your liking.",
            CONFIG_FILE_PATH.display()
        );
        std::process::exit(0);
    }
    Ok(())
}

/// Import existing config file
pub fn read_config() -> Result<Config> {
    let file = fs::read_to_string(&*CONFIG_FILE_PATH)?;
    let config: Config = serde_yaml::from_str(&file)?;
    Ok(config)
}
